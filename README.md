# qh-design-system

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/qh-design-system.svg)](https://www.npmjs.com/package/qh-design-system) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save qh-design-system
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'qh-design-system'
import 'qh-design-system/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [nanadjei](https://github.com/nanadjei)
