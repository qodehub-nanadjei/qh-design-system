import React, { Fragment, useState } from "react";
import { CheckedTab, Icon, Button, Grid } from "qh-design-system";
import { toast } from "react-toastify";

export default () => {
  const [active, setActive] = useState(0);

  const progress = (type) => {
    type
      ? type === "prev"
        ? active !== 0
          ? setActive((key) => key - 1)
          : toast("No progress exists to go back")
        : type === "next"
        ? active !== 6
          ? setActive((key) => key + 1)
          : setActive(0)
        : console.log("no type exist after next")
      : console.log("no type specified");
  };

  return (
    <Fragment>
      <section className="mb-12">
        <h3>Variant sm</h3>
        <CheckedTab
          activeKey={active}
          variant="sm"
          tabs={[
            {
              title: "First progress",
              desc: "First progress description",
              icon: Icon.ChevronRight,
            },
            {
              title: "Progress 2",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 3",
              desc: "description 3",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 4",
              desc: "description 4",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 5",
              desc: "description 5",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 6",
              desc: "description for item 6",
              icon: Icon.Appointment,
            },
          ]}
        />
        <Grid className="my-3" sm="repeat(2, 1fr)">
          <Button onClick={() => progress("prev")}>Previous</Button>
          <Button onClick={() => progress("next")}>Next</Button>
        </Grid>
      </section>

      <section className="my-12">
        <h3>Variant lg</h3>
        <CheckedTab
          activeKey={active}
          variant="lg"
          tabs={[
            {
              title: "First progress",
              desc: "First progress description",
              icon: Icon.ArrowDown,
            },
            {
              title: "Progress 2",
              icon: Icon.Direction,
            },
            {
              title: "Progress 3",
              desc: "description 3",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 4",
              desc: "description 4",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 5",
              desc: "description 5",
              icon: Icon.Appointment,
            },
            {
              title: "Progress 6",
              desc: "description for item 6",
              icon: Icon.Appointment,
            },
          ]}
        />

        <Grid className="my-3" sm="repeat(2, 1fr)">
          <Button onClick={() => progress("prev")}>Previous</Button>
          <Button onClick={() => progress("next")}>Next</Button>
        </Grid>
      </section>

      <section className="my-12">
        <h3>Variant icon</h3>
        <CheckedTab
          activeKey={active}
          variant="icon"
          tabs={[
            {
              title: "First progress",
              desc: "First progress description",
              icon: Icon.Check,
            },
            {
              title: "Progress 2",
              icon: Icon.Direction,
            },
            {
              title: "Progress 3",
              desc: "description 3",
              icon: Icon.CheckedBox,
            },
            {
              title: "Progress 4",
              desc: "description 4",
              icon: Icon.Copy,
            },
            {
              title: "Progress 5",
              desc: "description 5",
              icon: Icon.Date,
            },
            {
              title: "Progress 6",
              desc: "description for item 6",
              icon: Icon.Form,
            },
          ]}
        />

        <Grid className="my-3" sm="repeat(2, 1fr)">
          <Button onClick={() => progress("prev")}>Previous</Button>
          <Button onClick={() => progress("next")}>Next</Button>
        </Grid>
      </section>
    </Fragment>
  );
};
