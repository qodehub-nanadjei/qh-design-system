import React from "react";
import { Skeleton } from "qh-design-system";

const Skeletons = () => {
  return <Skeleton type="table" count={7} />;
};

export default Skeletons;
