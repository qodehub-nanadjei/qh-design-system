import React from "react";
import { Table, Th, Td } from "qh-design-system";

const Tables = () => {
  return (
    <div>
      <Table className="mb-12">
        <thead>
          <tr>
            <Th>This</Th>
            <Th>is</Th>
            <Th>a</Th>
            <Th>Table</Th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <Td>This</Td>
            <Td>is</Td>
            <Td>a</Td>
            <Td>table data</Td>
          </tr>
        </tbody>

        <tbody>
          <tr>
            <Td>This</Td>
            <Td>is</Td>
            <Td>a</Td>
            <Td>table data</Td>
          </tr>
        </tbody>
      </Table>

      <Table className="mb-12 table-bordered table-dark">
      <thead>
        <tr>
          <Th>This</Th>
          <Th>is</Th>
          <Th>a</Th>
          <Th>Table</Th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <Td>This</Td>
          <Td>is</Td>
          <Td>a</Td>
          <Td>table data</Td>
        </tr>
      </tbody>
    </Table>

    <Table className="mb-12 table">
      <thead className="table-header-info">
        <tr>
          <Th>This</Th>
          <Th>is</Th>
          <Th>a</Th>
          <Th>Table</Th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <Td>This</Td>
          <Td>is</Td>
          <Td>a</Td>
          <Td>table data</Td>
        </tr>
      </tbody>
    </Table>

    <Table className="mb-12 table">
      <thead className="table-header-success">
        <tr>
          <Th>This</Th>
          <Th>is</Th>
          <Th>a</Th>
          <Th>Table</Th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <Td>This</Td>
          <Td>is</Td>
          <Td>a</Td>
          <Td>table data</Td>
        </tr>
      </tbody>
    </Table>

    <Table className="mb-12 table">
      <thead className="table-header-warning">
        <tr>
          <Th>This</Th>
          <Th>is</Th>
          <Th>a</Th>
          <Th>Table</Th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <Td>This</Td>
          <Td>is</Td>
          <Td>a</Td>
          <Td>table data</Td>
        </tr>
      </tbody>
    </Table>

    <Table className="mb-12 table">
      <thead className="table-header-danger">
        <tr>
          <Th>This</Th>
          <Th>is</Th>
          <Th>a</Th>
          <Th>Table</Th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <Td>This</Td>
          <Td>is</Td>
          <Td>a</Td>
          <Td>table data</Td>
        </tr>
      </tbody>
    </Table>
    </div>
  );
};

export default Tables;
