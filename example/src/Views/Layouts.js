import React from "react";
import { Layout } from "nss-react-lib";
import { BrowserRouter, Switch } from "react-router-dom";

const Layouts = () => {
  return (
    <Switch>
      <Layout back={{ link: "", type: "arrow" }} items={<div />} />
    </Switch>
  );
};

export default Layouts;
