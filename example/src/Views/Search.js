import React from "react";
import { Searcher, Grid } from "nss-react-lib";

const Search = () => {
  const options = [
    {
      label: "Abigail Edwin",
      value: "abigail-edwin",
    },
  ];

  return (
    <Grid gap="24px">
      <Searcher
        withIcon
        placeholder="Type to search for something"
        options={options}
      />
      <Searcher placeholder="Type to search for something" options={options} />
    </Grid>
  );
};

export default Search;
