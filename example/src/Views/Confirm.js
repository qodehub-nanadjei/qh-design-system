import React from "react";
import { toast } from "react-toastify";
import { Grid, Button, Confirm, Notify } from "qh-design-system";

const Confirms = () => {
  const handleConfirm = async (e) => {
    e === "default" &&
      (await Confirm({
        msg: "Are you sure you want to confirm these records?",
      }).catch((err) => {
        console.error("Cancelled");
      }));

    if (e) {
      toast(<Notify body="No records exist" type="error" />);
      return;
    }

    if (
      await Confirm({
        header: "Confirm approval",
        msg: "Are you sure you want to approve these records?",
        buttons: {
          proceed: {
            children: "Approve",
          },
        },
      })
    ) {
      console.log("Approved");
      toast(<Notify body="All records have been approved" type="success" />);
    } else {
      console.log("Cancelled approval");
    }
  };

  return (
    <Grid sm="repeat(3, 1fr)">
      <Button onClick={() => handleConfirm("default")}>Confirm</Button>
      <Button onClick={() => handleConfirm()}>Confirm (custom)</Button>
    </Grid>
  );
};

export default Confirms;
