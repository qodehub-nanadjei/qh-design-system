import React from "react";
import { Grid, Icon } from "qh-design-system";

const Icons = () => {
  return (
    <Grid sm="1fr" flow="row">
      <div>
        <h3 className="mb-2">Arrows</h3>
        <Grid sm="repeat(4, 1fr)">
          <div>
            <Icon.ArrowDown />
            <p className="text-sub">Arrow Down</p>
          </div>
          <div>
            <Icon.ArrowLeft />
            <p className="text-sub">Arrow Left</p>
          </div>
          <div>
            <Icon.ArrowRight />
            <p className="text-sub">Arrow Right</p>
          </div>
          <div>
            <Icon.ArrowUp />
            <p className="text-sub">Arrow Up</p>
          </div>
        </Grid>
      </div>
      <div>
        <h3 className="mb-2">Chevron</h3>
        <Grid sm="repeat(4, 1fr)">
          <div>
            <Icon.ChevronDown />
            <p className="text-sub">Chevron Down</p>
          </div>
          <div>
            <Icon.ChevronLeft />
            <p className="text-sub">Chevron Left</p>
          </div>
          <div>
            <Icon.ChevronRight />
            <p className="text-sub">Chevron Right</p>
          </div>
          <div>
            <Icon.ChevronUp />
            <p className="text-sub">Chevron Up</p>
          </div>
          <div>
            <Icon.ChevronDown variant="round" />
            <p className="text-sub">Chevron Down Round</p>
          </div>
          <div>
            <Icon.ChevronLeft variant="round" />
            <p className="text-sub">Chevron Left Round</p>
          </div>
          <div>
            <Icon.ChevronRight variant="round" />
            <p className="text-sub">Chevron Right Round</p>
          </div>
          <div>
            <Icon.ChevronUp variant="round" />
            <p className="text-sub">Chevron Up Round</p>
          </div>
        </Grid>
      </div>
      <div>
        <h3 className="mb-2">Others</h3>
        <Grid sm="repeat(4, 1fr)">
          <div>
            <Icon.Add />
            <p className="text-sub">Add</p>
          </div>
          <div>
            <Icon.Appointment />
            <p className="text-sub">Appointment</p>
          </div>
          <div>
            <Icon.Archive />
            <p className="text-sub">Archive</p>
          </div>
          <div>
            <Icon.Bank />
            <p className="text-sub">Bank</p>
          </div>
          <div>
            <Icon.Cards />
            <p className="text-sub">Cards</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.Cart />
              <p className="text-sub">Cart</p>
            </div>
            <div>
              <Icon.Cart variant="active" />
              <p className="text-sub">Cart Active</p>
            </div>
          </Grid>
          <div>
            <Icon.Categorize />
            <p className="text-sub">Categorize</p>
          </div>
          <div>
            <Icon.Categorized />
            <p className="text-sub">Categorized</p>
          </div>
          <div>
            <Icon.Certificate />
            <p className="text-sub">Certificate</p>
          </div>
          <div>
            <Icon.Check />
            <p className="text-sub">Check</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.CheckedBox />
              <p className="text-sub">CheckedBox</p>
            </div>
            <div>
              <Icon.CheckedBox variant="round" />
              <p className="text-sub">CheckedBox Round</p>
            </div>
          </Grid>
          <Grid gap="24px" sm="auto auto auto">
            <div>
              <Icon.Close />
              <p className="text-sub">Close</p>
            </div>
            <div>
              <Icon.Close variant="round" />
              <p className="text-sub">Close round</p>
            </div>
            <div>
              <Icon.Close variant="filled" />
              <p className="text-sub">Close filled</p>
            </div>
          </Grid>
          <div>
            <Icon.Copy />
            <p className="text-sub">Copy</p>
          </div>
          <div>
            <Icon.Date />
            <p className="text-sub">Copy</p>
          </div>
          <div>
            <Icon.Direction />
            <p className="text-sub">Direction</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.Download />
              <p className="text-sub">Download</p>
            </div>
            <div>
              <Icon.Download variant="round" />
              <p className="text-sub">Download Round</p>
            </div>
          </Grid>
          <div>
            <Icon.Edit />
            <p className="text-sub">Edit</p>
          </div>
          <div>
            <Icon.Enrolment />
            <p className="text-sub">Enrolment</p>
          </div>
          <div>
            <Icon.Evaluation />
            <p className="text-sub">Evaluation</p>
          </div>
          <div>
            <Icon.Ezwich />
            <p className="text-sub">Ezwich</p>
          </div>
          <div>
            <Icon.File />
            <p className="text-sub">File</p>
          </div>
          <div>
            <Icon.Filter />
            <p className="text-sub">Filter</p>
          </div>
          <div>
            <Icon.Form />
            <p className="text-sub">Form</p>
          </div>
          <div>
            <Icon.Gear />
            <p className="text-sub">Gear</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.Help />
              <p className="text-sub">Help</p>
            </div>
            <div>
              <Icon.Help color="var(--gull-grey)" variant="fill" />
              <p className="text-sub">Help Fill</p>
            </div>
          </Grid>
          <div>
            <Icon.ID />
            <p className="text-sub">ID</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.Info />
              <p className="text-sub">Info</p>
            </div>
            <div>
              <Icon.Info variant="fill" />
              <p className="text-sub">Info fill</p>
            </div>
          </Grid>
          <div>
            <Icon.Logout />
            <p className="text-sub">Logout</p>
          </div>
          <div>
            <Icon.MapPointer />
            <p className="text-sub">Map Pointer</p>
          </div>
          <div>
            <Icon.Marketplace />
            <p className="text-sub">Marketplace</p>
          </div>
          <div>
            <Icon.Menu />
            <p className="text-sub">Menu</p>
          </div>
          <div>
            <Icon.Message />
            <p className="text-sub">Message</p>
          </div>
          <div>
            <Icon.Mobile />
            <p className="text-sub">Mobile</p>
          </div>
          <div>
            <Icon.Money />
            <p className="text-sub">Money</p>
          </div>
          <div>
            <Icon.MoreHoriz />
            <p className="text-sub">MoreHoriz</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.Notification />
              <p className="text-sub">Notification</p>
            </div>
            <div>
              <Icon.Notification variant="active" />
              <p className="text-sub">Notification</p>
            </div>
          </Grid>
          <div>
            <Icon.Overview />
            <p className="text-sub">Overview</p>
          </div>
          <div>
            <Icon.Personnel />
            <p className="text-sub">Personnel</p>
          </div>
          <div>
            <Icon.Photo />
            <p className="text-sub">Photo</p>
          </div>
          <div>
            <Icon.Pin />
            <p className="text-sub">Pin</p>
          </div>
          <div>
            <Icon.Posting />
            <p className="text-sub">Posting</p>
          </div>
          <div>
            <Icon.Print />
            <p className="text-sub">Print</p>
          </div>
          <div>
            <Icon.Profile />
            <p className="text-sub">Profile</p>
          </div>
          <div>
            <Icon.Register />
            <p className="text-sub">Register</p>
          </div>
          <div>
            <Icon.Remove />
            <p className="text-sub">Remove</p>
          </div>
          <div>
            <Icon.Request />
            <p className="text-sub">Request</p>
          </div>
          <div>
            <Icon.Reshuffle />
            <p className="text-sub">Reshuffle</p>
          </div>
          <div>
            <Icon.Search />
            <p className="text-sub">Search</p>
          </div>
          <div>
            <Icon.Star />
            <p className="text-sub">Star</p>
          </div>
          <div>
            <Icon.ThumbsUp />
            <p className="text-sub">ThumbsUp</p>
          </div>
          <div>
            <Icon.Time />
            <p className="text-sub">Time</p>
          </div>
          <div>
            <Icon.Uncategorized />
            <p className="text-sub">Uncategorized</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.UncheckedBox />
              <p className="text-sub">UncheckedBox</p>
            </div>
            <div>
              <Icon.UncheckedBox variant="round" />
              <p className="text-sub">UncheckedBox Round</p>
            </div>
          </Grid>
          <div>
            <Icon.Upload />
            <p className="text-sub">Upload</p>
          </div>
          <div>
            <Icon.UserDetails />
            <p className="text-sub">User Details</p>
          </div>
          <Grid gap="24px" sm="auto auto">
            <div>
              <Icon.Visibility variant="on" />
              <p className="text-sub">Visibility On</p>
            </div>
            <div>
              <Icon.Visibility />
              <p className="text-sub">Visibility Off</p>
            </div>
          </Grid>
        </Grid>
      </div>
    </Grid>
  );
};

export default Icons;
