import React from "react";
import { Grid, Button, Icon } from "qh-design-system";
import { Fragment } from "react-is";

const Buttons = () => {
  return (
    <Fragment>
      <Grid sm="repeat(3, 1fr)">
      <Button>Button</Button>
      <Button isValid={false}>
        <Icon.ChevronLeft color="#fff" />
        <span className="ml-1">Button</span>
      </Button>
      <Button isSubmitting>Button</Button>

      <Button outline>Button</Button>
      <Button outline isValid={false} color="var(--info)">
        <Icon.ChevronLeft color="var(--info)" />
        <span className="ml-1">Button</span>
      </Button>
      <Button isSubmitting outline loadingColor="var(--info)">
        Button
      </Button>

      <Button isValid round>
        <Icon.ArrowRight color="#fff" />
      </Button>
      <Button round isValid={false}>
        <Icon.ArrowRight color="#fff" />
      </Button>
      <Button isSubmitting round>
        <Icon.ArrowRight />
      </Button>

      <Button isValid round outline>
        <Icon.ArrowRight color="var(--info)" />
      </Button>
      <Button round outline isValid={false}>
        <Icon.ArrowRight color="var(--info)" />
      </Button>
      <Button round outline isSubmitting loadingColor="var(--info)">
        <Icon.ArrowRight />
      </Button>
    </Grid>

    <div className="py-8">
      <Grid sm="repeat(4, 1fr)">
          <Button className="btn--sm btn--primary">Primary</Button>
          <Button className="btn--sm btn--warning">Warning</Button>
          <Button className="btn--sm btn--success">Success</Button>
          <Button className="btn--sm btn--danger">Danger</Button>
      </Grid>
    </div>

    <div className="py-8">
      <Grid sm="repeat(4, 1fr)">
          <Button outline className="btn--sm outline--primary">Primary</Button>
          <Button outline className="btn--sm outline--warning">Warning</Button>
          <Button outline className="btn--sm outline--success">Success</Button>
          <Button outline className="btn--sm outline--danger">Danger</Button>
      </Grid>
    </div>
    </Fragment>
  );
};

export default Buttons;
