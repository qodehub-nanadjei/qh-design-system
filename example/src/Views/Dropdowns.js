import React from "react";
import { Icon } from "qh-design-system";
import Dropdown from "react-bootstrap/Dropdown";

const Dropdowns = () => {
  return (
    <Dropdown>
      <Dropdown.Toggle>
        <span className="mr-1">Dropdown</span>
        <Icon.ChevronDown />
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Dropdown.Item>Hello world This is really good</Dropdown.Item>
        <Dropdown.Item>Hi world</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default Dropdowns;
