import React from "react";
import { Alert, Grid, Icon } from "qh-design-system";

export default () => {
  return (
    <div className="nav-tabs-container hide-scroll">
      <Grid sm="1fr">
        <section className="primary">
          <p className="title__primary mb-2">Primary</p>
          <Grid gap="1rem">
            <Alert variant="primary">Alert variant primary</Alert>
            <Alert variant="primary">
              <Icon.Info variant="fill" />
              Alert with Icon
            </Alert>
            <Alert variant="primary" withBorder>
              <Icon.Info variant="fill" />
              Alert with Icon and Border
            </Alert>
          </Grid>
        </section>
        <section className="primary">
          <p className="title__primary mb-2">Primary</p>
          <Grid gap="1rem">
            <Alert variant="primary">Alert variant primary</Alert>
          </Grid>
        </section>
        <section className="success">
          <p className="title__success mb-2">Success</p>
          <Grid gap="1rem">
            <Alert variant="success">Alert variant success</Alert>
          </Grid>
        </section>
        <section className="warning">
          <p className="title__warning mb-2">Warning</p>
          <Grid gap="1rem">
            <Alert variant="warning">Alert variant warning</Alert>
          </Grid>
        </section>
        <section className="danger">
          <p className="title__danger mb-2">Danger</p>
          <Grid gap="1rem">
            <Alert variant="danger">Alert variant danger</Alert>
          </Grid>
        </section>
        <section className="danger">
          <p className="title__danger mb-2">Danger Dismissable</p>
          <Grid gap="1rem">
            <Alert variant="danger" dismissable>Dismissable Alert variant danger
            </Alert>
          </Grid>
        </section>
      </Grid>
    </div>
  );
};
