import React from "react";
import { ProgressBar } from "qh-design-system";
const Progress = () => {
  return (
    <div>
      <ProgressBar className="pb-10" progressColor={'var(--info)'} height={10} width={10} borderRadius={20}/>
      <ProgressBar className="pb-10" progressColor={'var(--success)'} height={10} width={30} borderRadius={20}/>
      <ProgressBar className="pb-10" progressColor={'var(--warning)'} height={10} width={50} borderRadius={20}/>
      <ProgressBar className="pb-10" progressColor={'var(--danger)'} height={10} width={75} borderRadius={20}/>
    </div>
    
  );
};

export default Progress;
