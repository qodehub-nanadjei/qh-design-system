import React from "react";
import { Label } from "qh-design-system";
import JoeAvatar from "../img/joe.jpg";
import Elliot from "../img/elliot.jpg";
import Veronika from "../img/veronika.jpg";

const Labels = () => {
  return (
    <div>
      <Label 
      className='pb-6'
      avatarUrl={JoeAvatar} 
      textColor={'var(--dark)'}
      >
      Elvis
    </Label>
    
    <div className="d-flex">
      <Label 
      className='pr-6'
      bgColor={'var(--success)'}
      avatarUrl={Elliot} 
      textColor={'var(--white)'}
      detail={'Agent'}
      >
      Elliot
    </Label>

    <Label 
      className='pr-6'
      bgColor={'var(--info)'}
      avatarUrl={Veronika} 
      textColor={'var(--white)'}
      detail={'Friend'}
      dismissable
      >
      Joe
    </Label>
    <Label 
      className='pr-6'
      bgColor={'var(--warning)'}
      avatarUrl={JoeAvatar} 
      textColor={'var(--white)'}
      detail={'Admin'}
      >
      Joe
    </Label>
    </div>
    </div>
  );
};

export default Labels;
