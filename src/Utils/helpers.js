import moment from "moment";

export const minDate = (date = 60) =>
  moment().startOf("year").subtract(date, "years").toDate();

export const maxDate = (date = 16) =>
  moment().endOf("year").subtract(date, "years").toDate();
