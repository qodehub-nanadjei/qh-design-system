const uploadFileService = (url, payload) => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open("PUT", url, true);
    xhr.send(payload);
    xhr.onload = function () {
      this.status === 200 ? resolve() : reject(this.responseText);
    };
  });
};

export default uploadFileService;
