import {
  isValidPhoneNumber,
  isPossiblePhoneNumber,
} from "react-phone-number-input";
import { string, number, mixed } from "yup";

/**
 * validation object for string
 */
export const requireString = (field, required = true, schema) => {
  schema = schema || string();
  return required ? schema.required(`${field} is required`) : schema;
};

/**
 * validation object for number
 */
export const requireNumber = (field, required = true, schema) => {
  schema = (schema || number()).typeError("Only numbers allowed");
  return required ? schema.required(`${field} is required`) : schema;
};

/**
 * validation object for phone number
 */
export const requirePhoneNumber = (field, required = true, schema) => {
  return (schema || string()).test(
    "isValidPhone",
    `Enter valid ${field}`,
    (value) =>
      value
        ? isValidPhoneNumber(value) && isPossiblePhoneNumber(value)
        : !required,
  );
};

/**
 * validation object for conditions
 */
export const requireWhen = (dependencyField, field) =>
  string().when(`${dependencyField}`, (fld, schema) =>
    fld ? schema.required(`${field} is required`) : schema,
  );

/**
 * validation object for test conditions
 */
export const requireTest = (field, condition) =>
  string().test("require", `${field} is required`, (value) => condition(value));

/**
 * validation object for email
 */
export const requireEmail = (field, required = true, schema) => {
  schema = (schema || string()).email("Enter a valid email");
  return required ? schema.required(`${field} is required`) : schema;
};

/**
 * validation object for file upload
 */
export const requireFile = (
  field,
  type = [],
  required = true,
  size = 2,
  schema,
) => {
  const format = (() => {
    let format = [];
    if (type.includes("image")) {
      format = [
        ...format,
        ...["image/jpg", "image/jpeg", "image/png", "image/webp"],
      ];
    }

    if (type.includes("pdf")) {
      format = [...format, "application/pdf"];
    }

    if (type.includes("csv")) {
      format = [
        ...format,
        "text/csv",
        "text/x-csv",
        "application/vnd.ms-excel",
      ];
    }

    return format;
  })();

  return (schema || mixed())
    .test("fileName", `${field} is required`, (value) => !!value || !required)
    .test("fileSize", `${field} size is too large`, (value) =>
      value ? value?.size <= size * 1000000 : !required,
    )
    .test("fileType", "Unsupported file format", (value) =>
      value
        ? format.length === 0
          ? true
          : format.includes(value?.type)
        : !required,
    );
};
