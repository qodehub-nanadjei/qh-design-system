import React from "react";
import PropTypes from "prop-types";
import uniqueId from "lodash/uniqueId";
import Trigger from "react-bootstrap/OverlayTrigger";
import Over from "react-bootstrap/Popover";

import Icon from "../Icons/Info";

/**
 * props definition
 */
const propTypes = {
  placement: PropTypes.string,
  children: PropTypes.any.isRequired,
  component: PropTypes.oneOfType([PropTypes.node, PropTypes.elementType]),
};

const defaultProps = {
  component: Icon,
  placement: "bottom",
};

const Popover = ({ placement, children, ...props }) => {
  return (
    <Trigger
      trigger={["hover", "focus", "click"]}
      placement={placement}
      overlay={
        <Over id={uniqueId("popover")}>
          <Over.Content>{children}</Over.Content>
        </Over>
      }
    >
      <props.component color="var(--gull-grey)" variant="fill" />
    </Trigger>
  );
};

Popover.propTypes = propTypes;
Popover.defaultProps = defaultProps;

export default Popover;
