import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import ChevronLeft from "../Icons/ChevronLeft";
import ChevronRight from "../Icons/ChevronRight";

/**
 * props definition
 */
const propTypes = {
  perPage: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};

const defaultProps = {
  page: 1,
  perPage: 10,
  setPage: () => {},
};

/**
 *
 * @param {*} perPage number of items per page
 * @param {*} total total number of items
 * @param {*} page current page
 * @param {*} setPage update setPage
 */
const Pagination = ({ perPage, total, page, setPage, direction, ...props }) => {
  page = page - 1;

  /**
   * variables
   */
  const data = page * perPage;

  const from = data + 1;
  const to = data + perPage > total ? total : data + perPage;

  const prev = page !== 0;
  const next = to !== total;

  /**
   * function
   */
  const handleToggle = (e, type) => {
    e.preventDefault();

    if (type === "next") {
      if (next) {
        setPage(page + 2);
      }
    } else {
      if (prev) {
        setPage(page);
      }
    }
  };

  return (
    <Wrapper {...{ ...props, direction }}>
      <p className="mb-0 text-sub gull-grey-text">
        {from}-{to} of {total}
      </p>

      <Buttons>
        <a href="/" onClick={(e) => handleToggle(e, "prev")}>
          <ChevronLeft
            color={prev ? "var(--subtle-grey)" : "var(--gull-grey)"}
          />
        </a>
        <a href="/" onClick={(e) => handleToggle(e, "next")}>
          <ChevronRight
            color={next ? "var(--subtle-grey)" : "var(--gull-grey)"}
          />
        </a>
      </Buttons>
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  display: flex;
  padding-left: 1rem;
  padding-right: 1rem;
  align-items: center;
  justify-content: ${({ direction }) => direction || "flex-start"};

  p {
    margin-right: 1rem;
  }

  @media (min-width: 768px) {
    padding-left: 0px;
    padding-right: 0px;
    justify-content: ${({ direction }) => direction || "flex-end"};
  }
`;

const Buttons = styled.div`
  display: flex;
  border-radius: 4px;
  align-items: center;
  border: solid 1px var(--athens-grey);

  a {
    display: flex;
    height: 34px;
    width: 34px;

    &:first-child {
      border-right: solid 1px var(--athens-grey);
    }

    svg {
      margin: auto;
    }
  }
`;

Pagination.propTypes = propTypes;
Pagination.defaultProps = defaultProps;

export default Pagination;
