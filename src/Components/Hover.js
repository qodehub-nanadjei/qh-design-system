import React, { Children, cloneElement, useState } from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  component: PropTypes.any,
};

const defaultProps = {
  component: "div",
};

/**
 *
 * @param {*} setHover function to set hover
 */
const Hover = ({ component, children, ...props }) => {
  /**
   * variables
   */
  const Component = component;

  /**
   * states
   */
  const [play, setPlay] = useState("false");

  return (
    <Component
      {...props}
      onMouseEnter={() => setPlay("true")}
      onMouseLeave={() => setPlay("false")}
    >
      {Children.map(children, (child) =>
        cloneElement(child, { ...child.props, play }),
      )}
    </Component>
  );
};

Hover.propTypes = propTypes;
Hover.defaultProps = defaultProps;

export default Hover;
