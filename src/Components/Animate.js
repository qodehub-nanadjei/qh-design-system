import React, { Fragment } from "react";
import Image from "react-bootstrap/Image";
import Lottie from "react-lottie";
import PropTypes from "prop-types";
import styled from "styled-components";

/**
 * props definition
 */
const propTypes = {
  src: PropTypes.string,
  play: PropTypes.string,
  options: PropTypes.object,
  data: PropTypes.object.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
};

const defaultProps = {
  width: 100,
  height: 100,
  play: "false",
  options: {},
};

/**
 *
 * @param {*} src initial image
 * @param {*} play show animation
 * @param {*} options addition configurations
 * @param {*} data the lottie file
 * @param {*} width width of lottie file
 * @param {*} height height of lottie file
 */

const Animate = ({
  src,
  play,
  loop,
  data,
  width,
  height,
  options,
  ...props
}) => {
  /**
   * variables
   */
  const defaultOptions = {
    loop,
    autoplay: true,
    animationData: data,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <Fragment>
      {play === "true" ? (
        <Wrapper {...props}>
          <Lottie
            width={width}
            height={height}
            className="m-0"
            options={defaultOptions}
          />
        </Wrapper>
      ) : (
        <Image src={src} fluid {...props} />
      )}
    </Fragment>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  div {
    margin: 0px !important;
  }
`;

Animate.propTypes = propTypes;
Animate.defaultProps = defaultProps;

export default Animate;
