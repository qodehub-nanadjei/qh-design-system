import React, { useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import PropTypes from "prop-types";
import styled from "styled-components";
import Image from "react-bootstrap/Image";

import ArrowLeft from "../Icons/ArrowLeft";
import useTitle from "../Hooks/useTitle";
import useWidth from "../Hooks/useWidth";
import Close from "../Icons/Close";

/**
 * prop definition
 */
const propTypes = {
  back: PropTypes.shape({
    link: PropTypes.func,
    type: PropTypes.oneOf(["arrow", "close"]),
  }),
  header: PropTypes.string,
  home: PropTypes.string,
  items: PropTypes.any,
};

const defaultProps = {
  header: "Portal | NSS",
  home: "/",
};

const Layout = ({ back, header, children, home, items, ...props }) => {
  useTitle(header);

  /**
   * variables
   */
  const width = useWidth();
  let bodyRef = useRef(null);

  useEffect(() => {
    if (bodyRef) {
      bodyRef.scrollTop = 0;
    }
  }, []);

  return (
    <Wrapper>
      <Navbar>
        <Container
          className={`position-relative h-100 d-flex justify-content-${
            items ? "between" : "center"
          } justify-content-md-${
            items ? "between" : "start"
          } align-items-center`}
        >
          {width < 768 && back && (
            <Back onClick={back.link}>
              {back.type === "arrow" && <ArrowLeft />}
              {back.type === "close" && <Close />}
            </Back>
          )}
          <Link to={home}>
            <Image src="/logo.png" />
          </Link>

          {items}
        </Container>
      </Navbar>
      <Body id="body" ref={(ref) => (bodyRef = ref)} {...props}>
        {children}
      </Body>
    </Wrapper>
  );
};

/**
 * states
 */
const Back = styled.a`
  position: absolute;
  padding: 1rem;
  left: 0px;
`;

const Navbar = styled.div`
  height: 100%;
  background-color: #fff;

  img {
    height: 36px;
  }

  @media (min-width: 768px) {
    height: 80px;

    img {
      height: 48px;
    }
  }
`;

const Body = styled.div`
  position: relative;
  overflow-y: auto;
  height: 100%;
`;

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-rows: 56px calc(100% - 56px);

  @media (min-width: 768px) {
    grid-template-rows: 80px calc(100% - 80px);
  }
`;

Layout.propTypes = propTypes;
Layout.defaultProps = defaultProps;

export default Layout;
