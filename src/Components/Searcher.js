import React, { useState, useEffect, Fragment, useCallback } from "react";
import { ErrorMessage } from "formik";
import PropTypes from "prop-types";
import debounce from "lodash/debounce";
import Dropdown from "react-bootstrap/Dropdown";
import styled from "styled-components";
import Form from "react-bootstrap/Form";

import Spinner from "./Spinner";
import Search from "../Icons/Search";

/**
 * props definition
 */

const propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSearch: PropTypes.func,
  loading: PropTypes.bool,
  withFormik: PropTypes.bool,
  options: PropTypes.array,
  placeholder: PropTypes.string,
  component: PropTypes.any,
};

const defaultProps = {
  loading: false,
  withIcon: false,
  withFormik: false,
  placeholder: "Type to search",
};

const Searcher = ({
  name,
  label,
  value,
  loading,
  onChange,
  onSearch,
  withIcon,
  component,
  withFormik,
  options: initialOptions,
  placeholder: initPlaceholder,
  ...props
}) => {
  /**
   * state
   */
  const [show, setShow] = useState(false);
  const [search, setSearch] = useState(null);
  const [options, setOptions] = useState(null);
  const [selected, setSelected] = useState();
  const [placeholder, setPlaceholder] = useState();

  /**
   * functions
   */
  const handleSearch = (value) => {
    setSearch(value);

    if (value) {
      const options = initialOptions?.filter(({ label }) =>
        label.trim().toLowerCase().includes(value.trim().toLowerCase()),
      );

      handleOptions(options);

      if (onSearch) {
        handleAPI(value);
      }
    } else {
      setOptions(initialOptions);
    }
  };

  const handleOptions = useCallback(
    debounce((options) => {
      setOptions(options);
    }, 50),
    [],
  );

  const handleAPI = useCallback(
    debounce((value) => {
      onSearch(value);
    }, 1500),
    [],
  );

  /**
   * effects
   */
  useEffect(() => {
    if (initialOptions?.length > 0) {
      setOptions(initialOptions);
    }
  }, [initialOptions]);

  useEffect(() => {
    if (!selected && value && options?.length > 0) {
      const option = options?.find((o) => o.value === value);
      if (option) {
        setSelected(option);
        setSearch(option.label);
      }
    }
  }, [value, options, selected]);

  useEffect(() => {
    if (!search && !show && selected) {
      setSearch(selected.label);
    }
  }, [search, show, selected]);

  /**
   * variables
   */
  const Render = component || DropdownItem;

  return (
    <Wrapper {...props}>
      <Form.Group className="mb-0">
        {label && <Form.Label>{label}</Form.Label>}
        <div className="input-container d-flex align-items-center px-0">
          {withIcon && (
            <div className="pl-4 pr-2">
              <Search color="var(--gull-grey)" />
            </div>
          )}
          <Form.Control
            value={search || ""}
            className={withIcon && `pl-0`}
            placeholder={placeholder || initPlaceholder}
            onChange={({ currentTarget: { value } }) => handleSearch(value)}
            onBlur={() => setShow(false) | setPlaceholder("")}
            onFocus={() => {
              setShow(true);
              if (selected) {
                setSearch("");
                setPlaceholder(selected.label);
              }
            }}
          />
        </div>
        {withFormik && (
          <ErrorMessage
            name={name}
            component="p"
            className="text-sub cinnabar-text ml-4 mb-0"
          />
        )}
      </Form.Group>
      <Menu show={show && (loading ? true : options)} className="dropdown-menu">
        {loading ? (
          <Dropdown.Item>
            <Spinner size={20} color="var(--radiant-blue)" />
          </Dropdown.Item>
        ) : (
          <Fragment>
            {options?.length > 0 && (
              <Fragment>
                {options.map((option, key) => (
                  <Render
                    key={key}
                    {...{
                      option,
                      selected,
                      onChange,
                      setSearch,
                      setOptions,
                      setSelected,
                      initialOptions,
                    }}
                  />
                ))}
              </Fragment>
            )}
            {options?.length === 0 && (
              <Dropdown.Item className="text-center text-body gull-grey-text">
                No options found
              </Dropdown.Item>
            )}
          </Fragment>
        )}
      </Menu>
    </Wrapper>
  );
};

/**
 * other components
 */
const DropdownItem = ({
  option,
  selected,
  onChange,
  setSearch,
  setOptions,
  setSelected,
  initialOptions,
  ...props
}) => (
  <Dropdown.Item
    {...props}
    className={`${selected?.value === option.value && "active"}`}
    onMouseDown={() => {
      setSelected(option);
      setSearch(option.label);
      setOptions(initialOptions);
      if (onChange) {
        onChange(option);
      }
    }}
  >
    {option.label}
  </Dropdown.Item>
);

/**
 * styles
 */
const Wrapper = styled.div`
  position: relative;
`;

const Menu = styled.div`
  margin-top: 8px;
  min-width: 200px;
  position: absolute;
  display: ${({ show }) => (show ? "block" : "none")};
`;

Searcher.propTypes = propTypes;
Searcher.defaultProps = defaultProps;

export default React.memo(Searcher);
