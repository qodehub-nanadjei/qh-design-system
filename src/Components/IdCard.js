import React, { Fragment } from "react";
import * as Card from "./IdCard/index";

const IdCard = ({ type, data }) => {
  return (
    <Fragment>
      {type === "SSNIT" && <Card.Snnit data={data} />}
      {type === "VOTERS_ID" && <Card.Voters data={data} />}
      {type === "DRIVERS_LICENSE" && <Card.Drivers data={data} />}
      {type === "NATIONAL_PASSPORT" && <Card.Passport data={data} />}
    </Fragment>
  );
};

export default IdCard;
