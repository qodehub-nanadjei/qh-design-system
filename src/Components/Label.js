import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Close from '../Icons/Close';

const propTypes = {
  bgColor: PropTypes.string,
  textColor: PropTypes.string,
  avatarUrl: PropTypes.string,
  children: PropTypes.any.isRequired,
  detail: PropTypes.string,
  dismissable: PropTypes.function
};

const Label = ({
  bgColor,
  textColor,
  avatarUrl,
  detail,
  dismissable,
  children,
  ...props
}) => {
  return (
    <Wrapper
      {...{
        bgColor,
        textColor,
        avatarUrl,
        detail,
        dismissable,
        ...props
      }}
    >
      <a className='label'>
        {avatarUrl ? <img src={avatarUrl} width='26' height='26' /> : ''}
        {children}
        {detail ? <div className='detail'>{detail}</div> : ''}
        {dismissable ? <Close size={12} className='label-delete-icon' /> : ''}
      </a>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  .label {
    color: ${({ textColor }) => textColor};
    background: ${({ bgColor }) => bgColor};
  }
  .label:hover {
    background-color: ${({ bgColor }) => bgColor} !important;
    border-color: ${({ bgColor }) => bgColor} !important;
    color: ${({ textColor }) => textColor} !important;
  }
  a.label .label-delete-icon {
    margin: ${({ dismissable, detail }) => {
      if(dismissable && detail) {
        return '0 0 0 1.5em';
      }
      return dismissable ? '0 0 0 0.75em' : '';
    }};
  }
`;

Label.prototype = propTypes;

export default Label;
