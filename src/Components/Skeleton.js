import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loading-skeleton-2';

import { Table, Th, Td } from './Table';
import loop from '../Utils/loop';

/**
 * props definition
 */
const propTypes = {
  type: PropTypes.oneOf(['table']).isRequired
};

/**
 *
 * @param {*} type type of skeleton to display
 */
const Skeleton = ({ type, ...props }) => {
  if (type === 'table') {
    return (
      <Table>
        <thead>
          <tr>
            {loop(props.count).map((key) => (
              <Th key={key}>
                <Loader width={112} height={22} />
              </Th>
            ))}
          </tr>
        </thead>
        <tbody>
          {loop(10).map((key) => (
            <tr key={key}>
              {loop(props.count).map((key) => (
                <Td key={key}>
                  <Loader width={144} height={22} />
                </Td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    );
  }

  return false;
};

Skeleton.propTypes = propTypes;

export default Skeleton;
