import React, { useRef } from 'react';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Form from 'react-bootstrap/Form';

import UploadIcon from '../Icons/Upload';
import FileIcon from '../Icons/File';
import useWidth from '../Hooks/useWidth';
import Popover from './Popover';
import Notify from '../Components/Notify';
import Close from '../Icons/Close';

/**
 * prop definitions
 */
const propTypes = {
  setFieldValue: PropTypes.func,
  accept: PropTypes.string,
  label: PropTypes.string.isRequired,
  max: PropTypes.number,
  value: PropTypes.any.isRequired,
  tip: PropTypes.string
};

const defaultProps = {
  withClear: false,
  max: 2
};

/**
 *
 * @param {*} setFieldValue function to set value
 * @param {*} name upload name
 * @param {*} value file to upload
 * @param {*} max max upload size
 * @param {*} label label for uploader
 * @param {*} accept input accept
 * @param {*} tip tip for information
 */
const Upload = ({
  setFieldValue,
  withClear,
  accept,
  label,
  value,
  max,
  tip,
  ...props
}) => {
  /**
   * ref
   */
  let img = useRef();

  const width = useWidth();

  /**
   * function
   */
  const handleUpload = (file) => {
    if (file) {
      const size = file?.size;

      if (size > max * 1000000) {
        toast(
          <Notify body={`File size must be less than ${max}mb`} type='error' />
        );
      } else {
        setFieldValue(file);
      }
    } else {
      img.value = '';
      setFieldValue('');
    }
  };

  return (
    <Wrapper>
      {value ? (
        <Form.Group className='mb-0 active-wrapper'>
          {label && (
            <Form.Label>
              <span>{label}</span>
            </Form.Label>
          )}
          <ActiveWrapper {...{ ...props, withClear }}>
            <div className='upload__content mr-auto'>
              <div className='upload__content__body text-truncate'>
                <FileIcon className='mr-3' color='var(--chateau-green)' />
                <p className='text-truncate text-body black-pearl-text'>
                  {value?.name}
                </p>
              </div>
            </div>
            {withClear && (
              <Close
                color='var(--gull-grey)'
                className='ml-3 cursor-pointer'
                onClick={() => handleUpload(null)}
              />
            )}
          </ActiveWrapper>
        </Form.Group>
      ) : (
        <Form.Group className='mb-0 inactive-wrapper'>
          {label && (
            <Form.Label className='d-flex justify-content-between'>
              <span>{label}</span>
              {tip && width < 767 && <Popover placement='top'>{tip}</Popover>}
            </Form.Label>
          )}
          <div className='d-flex align-items-center'>
            <InactiveWrapper onClick={() => img.click()} {...props}>
              <UploadIcon className='mr-3' color='var(--chateau-green)' />
              <span className='text-sub'>Browse file to upload</span>
            </InactiveWrapper>

            {tip && width >= 768 && (
              <span className='ml-2'>
                <Popover placement='top'>{tip}</Popover>
              </span>
            )}
          </div>
        </Form.Group>
      )}

      <input
        type='file'
        accept={accept}
        className='d-none'
        ref={(ref) => (img = ref)}
        onChange={({ currentTarget: { files } }) => handleUpload(files[0])}
      />
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  .inactive-wrapper,
  .active-wrapper {
    width: 100%;
  }

  @media (min-width: 768px) {
    .inactive-wrapper {
      max-width: 404px;
    }

    .active-wrapper {
      max-width: 364px;
    }
  }
`;

const ActiveWrapper = styled.div`
  display: ${({ withClear }) => (withClear ? 'flex' : 'inline-flex')};
  justify-content: space-between;
  align-items: center;
  margin-right: auto;
  max-width: 100%;
  height: 64px;

  .upload__content {
    width: ${({ withClear }) => (withClear ? 'calc(100% - 36px)' : '100%')};
    background-color: var(--desert-storm);
    border-radius: 8px;
    padding: 0.5rem;

    &__body {
      background-color: #fff;
      align-items: center;
      border-radius: 8px;
      padding: 12px 16px;
      display: flex;

      svg {
        flex: 0 0 24px;
      }
    }
  }

  @media (min-width: 768px) {
    .upload__content {
      max-width: 332px;
    }
  }
`;

const InactiveWrapper = styled.div`
  border: 1px dashed var(--gull-grey);
  background: var(--snow-drift);
  color: var(--chateau-green);
  justify-content: center;
  box-sizing: border-box;
  align-items: center;
  border-radius: 8px;
  cursor: pointer;
  display: flex;
  height: 72px;
  width: 100%;

  @media (min-width: 768px) {
    max-width: 364px;
  }
`;

Upload.propTypes = propTypes;
Upload.defaultProps = defaultProps;

export default Upload;
