import React from 'react';
import { createConfirmation, confirmable } from 'react-confirm';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';

import Button from './Button';
import Grid from './Grid';

/**
 * props definition
 */
const propTypes = {
  show: PropTypes.bool,
  header: PropTypes.string,
  msg: PropTypes.any.isRequired,
  proceed: PropTypes.func.isRequired
};

const Dialog = ({ show, proceed, msg, header, buttons }) => {
  buttons = {
    cancel: { outline: true, children: 'Cancel' },
    proceed: { children: 'Confirm' },
    ...buttons
  };

  return (
    <Modal onHide={() => proceed(false)} show={show} centered>
      <Modal.Body className='p-0 rounded overflow-hidden'>
        {header && (
          <div className='px-6 py-4 desert-storm-bg'>
            <h5 className='mb-0 font-weight-bold'>{header}</h5>
          </div>
        )}
        <div className='px-8 py-4'>
          <div className='mb-0 text-body'>{msg}</div>
        </div>
        <Grid
          gap='16px'
          sm='auto auto'
          className='justify-content-end py-4 px-6 athens-grey-border border-top'
        >
          <Button onClick={() => proceed(false)} {...buttons.cancel} />
          <Button onClick={() => proceed(true)} {...buttons.proceed} />
        </Grid>
      </Modal.Body>
    </Modal>
  );
};

Dialog.propTypes = propTypes;

const Confirm = createConfirmation(confirmable(Dialog));

export default Confirm;
