import React, { useEffect } from "react";
import { ErrorMessage } from "formik";
import ReactDatePicker from "react-datepicker";
import PropTypes from "prop-types";
import moment from "moment";
import styled from "styled-components";
import Form from "react-bootstrap/Form";

/**
 * picker prop definition
 */
const dateProps = PropTypes.oneOfType([
  PropTypes.instanceOf(Date),
  PropTypes.string,
]);

const pickerPropTypes = {
  setFieldValue: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  format: PropTypes.string,
  label: PropTypes.string,
  minDate: dateProps,
  maxDate: dateProps,
  value: dateProps.isRequired,
};

const pickerDefaultProps = {
  value: "",
  placeholder: "",
  setFieldValue: () => {},
};

/**
 * date picker
 *
 * @param {*} name form name for datepicker
 * @param {*} label label for form input
 * @param {*} value value for form input
 * @param {*} format date format
 * @param {*} maxDate max date the picker should accept
 * @param {*} minDate min date the picker should accept
 * @param {*} placeholder self explanatory
 * @param {*} setFieldValue function to set the date in formik
 */
const Picker = ({
  name,
  label,
  range,
  value,
  format,
  maxDate,
  minDate,
  placeholder,
  setFieldValue,
  ...props
}) => {
  const date = value ? new Date(value) : "";

  return (
    <Form.Group>
      {label && <Form.Label>{label}</Form.Label>}
      <div className={range ? "input-container d-flex align-items-center" : ""}>
        {range && <span className="text-body gull-grey-text">{range}</span>}
        <ReactDatePicker
          selected={date}
          showYearDropdown
          showMonthDropdown
          className="form-control"
          placeholderText={placeholder}
          onChange={(value) =>
            setFieldValue(name, value ? moment(value).format("YYYY-MM-DD") : "")
          }
          {...(maxDate && { maxDate: moment(maxDate).unix() * 1000 })}
          {...(minDate && { minDate: moment(minDate).unix() * 1000 })}
          {...props}
        />
      </div>
      <ErrorMessage
        name={name}
        component="p"
        className="text-sub cinnabar-text ml-4 mb-0"
      />
    </Form.Group>
  );
};

Picker.propTypes = pickerPropTypes;
Picker.defaultProps = pickerDefaultProps;

/**
 * range props definition
 */
const rangePropTypes = {
  setFieldValue: PropTypes.func.isRequired,
  names: PropTypes.arrayOf(PropTypes.string).isRequired,
  values: PropTypes.arrayOf(dateProps).isRequired,
  label: PropTypes.string.isRequired,
  minDate: dateProps,
  maxDate: dateProps,
};

/**
 * range picker
 * @param {*} names array of from and to names
 * @param {*} label label of the inputs
 * @param {*} values array of from and to values
 * @param {*} minDate self explanatory
 * @param {*} maxDate self explanatory
 * @param {*} setFieldValue function to set formik value
 */
const Range = ({ names, label, values, ...props }) => {
  /**
   * variables
   */
  const [fromName, toName] = names;
  const [fromValue, toValue] = values;

  /**
   * effect
   */
  useEffect(() => {
    if (fromValue && toValue) {
      if (moment(fromValue).isAfter(moment(toValue))) {
        props.setFieldValue(toName, "");
      }
    }
  }, [values]);

  return (
    <Form.Group>
      {label && <Form.Label>{label}</Form.Label>}
      <RangeWrapper className="input-container">
        <Picker name={fromName} value={fromValue} range="From" {...props} />
        <Picker
          range="To"
          name={toName}
          value={toValue}
          minDate={fromValue}
          {...props}
        />
      </RangeWrapper>
    </Form.Group>
  );
};

Range.propTypes = rangePropTypes;

/**
 * styles
 */
const RangeWrapper = styled.div`
  grid-template-columns: 50% 50%;
  display: grid;

  .input-container {
    padding: 0px;
    border: none;
    height: 46px;
    box-shadow: none;
  }

  .form-group {
    margin-bottom: 0px;

    .form-control {
      padding-right: 0px;
    }
  }
`;

/**
 * export functions
 */
export { Range, Picker };
