import React from "react";
import Tbl from "react-bootstrap/Table";
import PropTypes from "prop-types";

/**
 * props definition
 */
const childProps = {
  center: PropTypes.bool,
  right: PropTypes.bool,
  left: PropTypes.bool,
  children: PropTypes.any,
};

const Td = ({ center, right, left, children, className, ...props }) => {
  return (
    <td {...props}>
      <div
        className={`td text-wrap justify-content-${
          (center && "center") ||
          (right && "end") ||
          (left && "start") ||
          "start"
        } ${className ? className : ""}`}
      >
        {children}
      </div>
    </td>
  );
};

const Th = ({ children, left, right, className, center, ...props }) => {
  return (
    <th {...props}>
      <div
        className={`th justify-content-${
          (center && "center") ||
          (right && "end") ||
          (left && "start") ||
          "start"
        } ${className ? className : ""}`}
      >
        {children}
      </div>
    </th>
  );
};

const Table = ({ withPagination, children, ...props }) => {
  return (
    <div className={`table-responsive h-100 ${withPagination && "mb-4"}`}>
      <Tbl {...props}>{children}</Tbl>
    </div>
  );
};

Th.propTypes = childProps;
Td.propTypes = childProps;

export { Td, Th, Table };
