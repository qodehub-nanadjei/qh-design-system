import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import ChevronLeft from "../Icons/ChevronLeft";
import ArrowLeft from "../Icons/ArrowLeft";
import Close from "../Icons/Close";

/**
 * props definition
 */
const propTypes = {
  icon: PropTypes.oneOf(["close", "arrow", "chevron"]),
  rounded: PropTypes.bool,
};

const defaultProps = {
  icon: "close",
  rounded: false,
};

/**
 *
 * @param {*} icon icon type to use
 * @param {*} rounded gives icon a background rounded feel
 */
const Back = ({ icon, rounded, children, ...props }) => (
  <Wrapper {...{ ...props, rounded }}>
    {icon === "close" && <Close color="var(--subtle-grey)" />}
    {icon === "arrow" && <ArrowLeft color="var(--subtle-grey)" />}
    {icon === "chevron" && <ChevronLeft color="var(--subtle-grey)" />}
    {!rounded && children && (
      <span className="text-body subtle-grey-text">{children}</span>
    )}
  </Wrapper>
);

/**
 * styles
 */
const Wrapper = styled.a`
  text-decoration: none !important;
  cursor: pointer;
  display: none;

  span {
    margin-top: 1px;
    margin-left: 8px;
  }

  ${({ rounded }) =>
    rounded &&
    `
      background-color: #fff;
      justify-content: center;
      align-items: center;
      border-radius: 50%;
      height: 48px;
      width: 48px;
    `}

  @media (min-width: 768px) {
    grid-template-columns: 24px auto;
    display: grid;

    ${({ rounded }) =>
      rounded &&
      `
        display: flex !important;
    `}
  }
`;

Back.propTypes = propTypes;
Back.defaultProps = defaultProps;

export default Back;
