import React, { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Close from '../Icons/Close';

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  opacity: PropTypes.number,
  withBorder: PropTypes.bool,
  children: PropTypes.any.isRequired,
  variant: PropTypes.oneOf(['primary', 'success', 'warning', 'danger']),
  dismissable: PropTypes.bool
};

const defaultProps = {
  withBorder: false,
  dismissable: false,
  opacity: 0.1
};

/**
 *
 * @param {*} variant variant of alert
 * @param {*} withBorder show border
 * @param {*} color color of alert
 * @param {*} opacity opacity of alert background
 * @param {*} children content of alert
 */
const Alert = ({
  variant,
  withBorder,
  color,
  opacity,
  children,
  dismissable,
  ...props
}) => {
  /**
   * variables
   */
  color = ((variant, color) => {
    if (color) {
      return color;
    }

    switch (variant) {
      case 'primary':
        return 'var(--info)';
      case 'success':
        return 'var(--success)';
      case 'warning':
        return 'var(--warning)';
      case 'danger':
        return 'var(--danger)';
      default:
        break;
    }
  })(variant, color);

  // check whether children has icon
  const content = ((children, color) => {
    const content = Children.toArray(children);

    if (content.length === 2) {
      const icon = cloneElement(content[0], { color }, null);
      return { icon: icon, children: content[1] };
    } else {
      return { children: content[0] };
    }
  })(children, color);

  // const [showAlert, setShowAlert] = useState(true);

  // Handle dismissable
  const removeAlert = () => {
    // this.setShowAlert(false);
  }

  return (
    <Wrapper
      {...{
        color,
        opacity,
        withBorder,
        icon: content?.icon,
        dismissable,
        ...props
      }}
    >
      {content?.icon && content.icon}
      <p
        className={`mb-0 text-sub ${dismissable ? 'flex justify-between' : ''}`}
      >
        {content.children}
        <span
          className={!dismissable ? 'd-none' : 'close-btn'}
          onClick={() => removeAlert()}
        >
          {' '}
          <Close size={12} />
        </span>
      </p>
    </Wrapper>
  )
}

/**
 * styles
 */
const Wrapper = styled.div`
  grid-template-columns: ${({ icon }) => (icon ? '24px auto' : '100%')};
  position: relative;
  border-radius: 8px;
  padding: 12px 16px;
  overflow: hidden;
  display: grid;
  z-index: 1;
  gap: 12px;

  border: ${({ withBorder, color }) =>
    withBorder ? `solid 1px ${color}` : 'none'};

  &::before {
    z-index: -1;
    width: 100%;
    height: 100%;
    content: '';
    display: block;
    position: absolute;
    opacity: ${({ opacity }) => opacity};
    background-color: ${({ color }) => color};
  }

  p {
    color: var(--subtle-grey);
  }

  .d-none {
    display: none !important;
  }
  .close-btn {
    cursor: pointer;
  }

  .close-icon {
    width: 12;
  }

  .flex {
    display: flex;
  }

  .justify-between {
    justify-content: space-between;
  }
`

Alert.propTypes = propTypes;
Alert.defaultProps = defaultProps;

export default Alert;
