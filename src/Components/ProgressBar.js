import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const propTypes = {
  progressColor: PropTypes.string,
  borderRadius: PropTypes.number,
  color: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  progressHeight: PropTypes.number,
  animate: PropTypes.bool,
  animationTiming: PropTypes.number
};

const ProgressBar = ({
  width,
  height,
  progressColor,
  borderRadius,
  progressHeight,
  animate,
  animationTiming,
  color,
  ...props
}) => {
  return (
    <Wrapper
      {...{
        width,
        height,
        progressColor,
        borderRadius,
        progressHeight,
        animate,
        animationTiming,
        color,
        ...props
      }}
    >
      <div className='progress'>
        <div
          className='progress-bar'
          role='progressbar'
          style={{ width: `${width}%`, height: `${height || 1}px` }}
          aria-valuenow={width}
          aria-valuemin='0'
          aria-valuemax='100'
        />
      </div>
    </Wrapper>
  );
};

/**
 * Styles
 */
const Wrapper = styled.div`
  @keyframes progress-bar-stripes {
    from {
      background-position: ${({ height }) => height} 0;
    }
    to {
      background-position: 0 0;
    }
  }
  height: ${({ height }) => height};

  .progress {
    background-color: #e9ecef;
    border-radius: ${({ borderRadius }) => `${borderRadius}px` || '5px'};
  }

  .progress-bar {
    color: ${({ color }) => color};
    background-color: ${({ progressColor }) => progressColor};
  }

  .progress-bar-striped {
    background-size: ${({ progressHeight }) => progressHeight};
  }

  .progress-bar-animated {
    animation: progress-bar-stripes
      ${({ animate, animationTiming }) => (animate ? animationTiming : 'none')};
  }
`;

ProgressBar.prototype = propTypes;

export default ProgressBar;
