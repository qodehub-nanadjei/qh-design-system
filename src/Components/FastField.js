import React from "react";
import { FastField as Field, ErrorMessage } from "formik";
import Form from "react-bootstrap/Form";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  label: PropTypes.string,
  cProps: PropTypes.object,
};

/**
 *
 * @param {*} label field label
 * @param {*} cProps container props
 * @returns
 */
const FastField = ({ label, cProps, ...props }) => {
  return (
    <Form.Group controlId={props.name} {...cProps}>
      <Form.Label>{label}</Form.Label>
      <Field {...props} className={`form-control ${props.className}`} />
      <ErrorMessage
        name={props.name}
        className="text-sub cinnabar-text ml-4 mb-0"
        component="p"
      />
    </Form.Group>
  );
};

FastField.propTypes = propTypes;

export default FastField;
