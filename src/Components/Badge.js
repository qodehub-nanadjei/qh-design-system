import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * prop definition
 */
const propTypes = {
  bg: PropTypes.string,
  color: PropTypes.string,
  isStatus: PropTypes.bool,
  opacity: PropTypes.number,
  children: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(["failed", "success", "processing", "warning"]),
  radius: PropTypes.number,
};

const defaultProps = {
  color: "#fff",
  isStatus: false,
  opacity: 0.1,
  radius: 28,
};

/**
 *
 * @param {*} bg for badge background
 * @param {*} color text color
 * @param {*} children data in badge
 * @param {*} variant badge variant background and forground color
 * @param {*} isStatus whether the badge is a status badge or normal badge
 * @param {*} opacity status badge opacity if default not working
 */
const Badge = ({
  bg,
  color,
  radius,
  variant,
  opacity,
  isStatus,
  children,
  ...props
}) => {
  /**
   * variables
   */
  bg = ((bg, variant) => {
    if (bg) {
      return bg;
    }

    switch (variant) {
      case "failed":
        return "var(--cinnabar)";
      case "success":
        return "var(--chateau-green)";
      case "processing":
        return "var(--radiant-blue)";
      case "warning":
        return "var(--fire-bush)";
      default:
        break;
    }
  })(bg, variant);

  return (
    <Wrapper {...{ ...props, bg, color, isStatus, opacity }}>
      <span className="text-sub -medium">{children}</span>
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  z-index: 1;
  overflow: hidden;
  position: relative;
  display: inline-block;
  padding: 0.25rem 0.5rem;
  border-radius: ${({ radius }) => `${radius}px`};
  background-color: ${({ bg, isStatus }) => (isStatus ? "#fff" : bg)};

  span {
    color: ${({ isStatus, color, bg }) => (isStatus ? bg : color)};
  }

  &::before {
    display: ${({ isStatus }) => (isStatus ? "block" : "none")};
    opacity: ${({ opacity }) => opacity};
    background-color: ${({ bg }) => bg};
    position: absolute;
    content: " ";
    height: 100%;
    width: 100%;
    z-index: -1;
    right: 0;
    top: 0;
  }
`;

Badge.propTypes = propTypes;
Badge.defaultProps = defaultProps;

export default Badge;
