import { useState, useEffect } from "react";

const useWidth = () => {
  /**
   * state
   */
  const [width, setWidth] = useState(window.innerWidth);

  /**
   * function
   */
  const handleWidth = () => setWidth(window.innerWidth);

  /**
   * effect
   */
  useEffect(() => {
    window.addEventListener("resize", () => handleWidth());
    return () => window.removeEventListener("resize", handleWidth());
  });

  return width || handleWidth();
};

export default useWidth;
