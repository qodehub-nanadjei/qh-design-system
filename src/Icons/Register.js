import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Register = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5 1C3.89543 1 3 1.89543 3 3V21C3 22.1046 3.89543 23 5 23H6C6.55228 23 7 22.5523 7 22C7 21.4477 6.55228 21 6 21H5V3H19V5C19 5.55228 19.4477 6 20 6C20.5523 6 21 5.55228 21 5V3C21 1.89543 20.1046 1 19 1H5ZM8 6C7.44772 6 7 6.44772 7 7C7 7.55228 7.44772 8 8 8H14C14.5523 8 15 7.55228 15 7C15 6.44772 14.5523 6 14 6H8ZM7 11C7 10.4477 7.44772 10 8 10H10C10.5523 10 11 10.4477 11 11C11 11.5523 10.5523 12 10 12H8C7.44772 12 7 11.5523 7 11ZM21.6238 8.37623C20.6349 7.38739 19.0317 7.38739 18.0429 8.37623L9.17157 17.2475C8.42143 17.9977 8 19.0151 8 20.076V21C8 21.5523 8.44772 22 9 22H9.92403C10.9849 22 12.0023 21.5786 12.7525 20.8284L21.6238 11.9571C22.6126 10.9683 22.6126 9.36506 21.6238 8.37623ZM19.4571 9.79044C19.6649 9.58266 20.0018 9.58266 20.2096 9.79044C20.4173 9.99822 20.4173 10.3351 20.2096 10.5429L11.3382 19.4142C10.9815 19.771 10.5038 19.979 10.0015 19.9985C10.021 19.4962 10.229 19.0185 10.5858 18.6618L19.4571 9.79044Z"
      fill={color}
    />
  </svg>
);

Register.propTypes = propTypes;
Register.defaultProps = defaultProps;

export default Register;
