import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const UserDetails = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12 8C13.1046 8 14 7.10457 14 6C14 4.89543 13.1046 4 12 4C10.8954 4 10 4.89543 10 6C10 7.10457 10.8954 8 12 8ZM12 10C14.2091 10 16 8.20914 16 6C16 3.79086 14.2091 2 12 2C9.79086 2 8 3.79086 8 6C8 8.20914 9.79086 10 12 10ZM12 14C11.2121 14 10.4319 14.1552 9.7039 14.4567C8.97595 14.7583 8.31451 15.2002 7.75736 15.7574C7.20021 16.3145 6.75825 16.9759 6.45672 17.7039C6.15519 18.4319 6 19.2121 6 20C6 20.5523 5.55228 21 5 21C4.44772 21 4 20.5523 4 20C4 18.9494 4.20693 17.9091 4.60896 16.9385C5.011 15.9679 5.60028 15.086 6.34315 14.3431C7.08601 13.6003 7.96793 13.011 8.93853 12.609C9.90914 12.2069 10.9494 12 12 12C12.5523 12 13 12.4477 13 13C13 13.5523 12.5523 14 12 14ZM12 16C12 15.4477 12.4477 15 13 15H20C20.5523 15 21 15.4477 21 16C21 16.5523 20.5523 17 20 17H13C12.4477 17 12 16.5523 12 16ZM12 20C12 19.4477 12.4477 19 13 19H17C17.5523 19 18 19.4477 18 20C18 20.5523 17.5523 21 17 21H13C12.4477 21 12 20.5523 12 20Z"
      fill={color}
    />
  </svg>
);

UserDetails.propTypes = propTypes;
UserDetails.defaultProps = defaultProps;

export default UserDetails;
