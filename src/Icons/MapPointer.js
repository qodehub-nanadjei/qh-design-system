import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const MapPointer = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M11.7965 0.737122C7.48407 0.737122 3.97559 4.2456 3.97559 8.55801C3.97559 13.9783 11.8042 23.2371 11.8042 23.2371C11.8042 23.2371 19.6174 13.7117 19.6174 8.55801C19.6174 4.2456 16.109 0.737122 11.7965 0.737122ZM14.1562 10.848C13.5055 11.4985 12.6511 11.8238 11.7965 11.8238C10.942 11.8238 10.0873 11.4985 9.43689 10.848C8.1357 9.54692 8.1357 7.42985 9.43689 6.12866C10.067 5.49832 10.9051 5.15115 11.7965 5.15115C12.6879 5.15115 13.5259 5.49846 14.1562 6.12866C15.4574 7.42985 15.4574 9.54692 14.1562 10.848Z"
      fill={color}
    />
  </svg>
);

MapPointer.propTypes = propTypes;
MapPointer.defaultProps = defaultProps;

export default MapPointer;
