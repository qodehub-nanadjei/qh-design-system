import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const ThumbsUp = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M13.2894 1C12.063 1 11.0071 1.86564 10.7666 3.06823C10.3181 5.31047 9.21598 7.36981 7.59908 8.98671L7.19195 9.39384C6.85907 9.1464 6.44663 9 6 9H4C2.89543 9 2 9.89543 2 11V19C2 20.1046 2.89543 21 4 21H6C6.66205 21 7.24897 20.6783 7.61298 20.1827C8.37015 20.7052 9.28072 21 10.2361 21H18C20.2091 21 22 19.2091 22 17V12C22 9.79086 20.2091 8 18 8H16V2.58835C16 1.71113 15.2889 1 14.4117 1H13.2894ZM8 17.7437C8.47214 18.5194 9.3176 19 10.2361 19H18C19.1046 19 20 18.1046 20 17V12C20 10.8954 19.1046 10 18 10H15C14.4477 10 14 9.55228 14 9V3H13.2894C13.0163 3 12.7813 3.19272 12.7277 3.46046C12.2018 6.08989 10.9094 8.50482 9.01329 10.4009L8 11.4142V17.7437ZM6 11H4L4 19H6V18V11Z"
      fill={color}
    />
  </svg>
);

ThumbsUp.propTypes = propTypes;
ThumbsUp.defaultProps = defaultProps;

export default ThumbsUp;
