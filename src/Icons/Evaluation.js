import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Evaluation = ({ size, color, ...props }) => {
  return (
    <svg
      fill="none"
      width={size}
      height={size}
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.7071 4.29289C17.0976 4.68342 17.0976 5.31658 16.7071 5.70711L10.4142 12L12 13.5858L20.2929 5.29289C20.6834 4.90237 21.3166 4.90237 21.7071 5.29289C22.0976 5.68342 22.0976 6.31658 21.7071 6.70711L12.7071 15.7071C12.5196 15.8946 12.2652 16 12 16C11.7348 16 11.4804 15.8946 11.2929 15.7071L9 13.4142L7.70711 14.7071C7.31658 15.0976 6.68342 15.0976 6.29289 14.7071L2.29289 10.7071C1.90237 10.3166 1.90237 9.68342 2.29289 9.29289C2.68342 8.90237 3.31658 8.90237 3.70711 9.29289L7 12.5858L8.29291 11.2929L15.2929 4.29289C15.6834 3.90237 16.3166 3.90237 16.7071 4.29289ZM4 18C3.44772 18 3 18.4477 3 19C3 19.5523 3.44772 20 4 20L20 20C20.5523 20 21 19.5523 21 19C21 18.4477 20.5523 18 20 18L4 18Z"
        fill={color}
      />
    </svg>
  );
};

Evaluation.propTypes = propTypes;
Evaluation.defaultProps = defaultProps;

export default Evaluation;
