import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Categorized = ({ color, size, ...props }) => {
  return (
    <svg
      fill="none"
      width={size}
      height={size}
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#clip0)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M21.7071 1.70711C22.0976 1.31658 22.0976 0.683417 21.7071 0.292893C21.3166 -0.0976311 20.6834 -0.0976311 20.2929 0.292893L17.5858 3H5C3.89543 3 3 3.89543 3 5V5.92154C3 6.12838 3.04816 6.33239 3.14066 6.51739C4.31471 8.86548 5.94623 10.9501 7.93488 12.6509L2.29289 18.2929C1.90237 18.6834 1.90237 19.3166 2.29289 19.7071C2.68342 20.0976 3.31658 20.0976 3.70711 19.7071L21.7071 1.70711ZM9.35388 11.2319L15.5858 5H5V5.76238C6.07247 7.86059 7.55381 9.71977 9.35388 11.2319ZM11 18.382V14.4142L9 16.4142V20C9 20.3466 9.17945 20.6684 9.47427 20.8507C9.76909 21.0329 10.1372 21.0494 10.4472 20.8944L13.8944 19.1708C14.572 18.832 15 18.1395 15 17.382V13.5L15.0851 13.4362C17.5208 11.6094 19.4977 9.24061 20.8593 6.51739C20.9518 6.33238 21 6.12838 21 5.92154V5C21 4.82084 20.9764 4.64718 20.9323 4.48196L18.1126 7.30162C16.9854 9.04696 15.5553 10.5835 13.8851 11.8362L13.5458 12.0907C13.2022 12.3483 13 12.7528 13 13.1822V17.382L11 18.382Z"
          fill={color}
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width="24" height="24" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

Categorized.propTypes = propTypes;
Categorized.defaultProps = defaultProps;

export default Categorized;
