import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Archive = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M4.27639 5.21115C4.95396 3.85601 6.33901 3 7.8541 3H16.1459C17.661 3 19.046 3.85601 19.7236 5.21115L20.5777 6.91935C20.8554 7.47477 21 8.08722 21 8.7082V17C21 19.2091 19.2091 21 17 21H7C4.79086 21 3 19.2091 3 17V8.7082C3 8.08722 3.14458 7.47477 3.42229 6.91935L4.27639 5.21115ZM7.8541 5C7.09656 5 6.40403 5.428 6.06525 6.10557L5.61803 7H18.382L17.9348 6.10557C17.596 5.428 16.9034 5 16.1459 5H7.8541ZM5 17V9H19V17C19 18.1046 18.1046 19 17 19H7C5.89543 19 5 18.1046 5 17ZM13 12C13 11.4477 12.5523 11 12 11C11.4477 11 11 11.4477 11 12V13.5858L10.7071 13.2929C10.3166 12.9024 9.68342 12.9024 9.29289 13.2929C8.90237 13.6834 8.90237 14.3166 9.29289 14.7071L11.2929 16.7071C11.6834 17.0976 12.3166 17.0976 12.7071 16.7071L14.7071 14.7071C15.0976 14.3166 15.0976 13.6834 14.7071 13.2929C14.3166 12.9024 13.6834 12.9024 13.2929 13.2929L13 13.5858V12Z"
      fill={color}
    />
  </svg>
);

Archive.propTypes = propTypes;
Archive.defaultProps = defaultProps;

export default Archive;
