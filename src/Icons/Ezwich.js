import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Gear = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M17.1707 7H20V9C18.6938 9 17.5825 8.16519 17.1707 7ZM15 5H17H20C21.1046 5 22 5.89543 22 7V17C22 18.1046 21.1046 19 20 19H17H15H9H7H4C2.89543 19 2 18.1046 2 17V7C2 5.89543 2.89543 5 4 5H7H9H15ZM8.89998 7H15.1C15.5633 9.28224 17.581 11 20 11V13C17.581 13 15.5633 14.7178 15.1 17H8.89998C8.43671 14.7178 6.41896 13 4 13V11C6.41896 11 8.43671 9.28224 8.89998 7ZM6.82929 7H4V9C5.30622 9 6.41746 8.16519 6.82929 7ZM4 15V17H6.82929C6.41746 15.8348 5.30622 15 4 15ZM17.1707 17H20V15C18.6938 15 17.5825 15.8348 17.1707 17Z"
      fill={color}
    />
  </svg>
);

Gear.propTypes = propTypes;
Gear.defaultProps = defaultProps;

export default Gear;
