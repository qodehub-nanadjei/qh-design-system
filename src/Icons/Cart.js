import React, { Fragment } from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  variant: PropTypes.oneOf(["active", "default"]),
  size: PropTypes.number,
  color: PropTypes.string,
};

const defaultProps = {
  size: 24,
  variant: null,
  color: "var(--black-pearl)",
};

const Cart = ({ color, variant, size, ...props }) => {
  return (
    <React.Fragment>
      <svg
        fill="none"
        width={size}
        height={size}
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        {variant === "active" ? (
          <Fragment>
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M8.00003 2C6.89546 2 6.00003 2.89543 6.00003 4V5H5.15979C3.99477 5 3.07632 5.99181 3.16568 7.1534L3.71594 14.3068C3.87625 16.3908 5.61401 18 7.70416 18H16.2959C18.3861 18 20.1238 16.3908 20.2841 14.3068L20.8344 7.15339C20.9237 5.9918 20.0053 5 18.8403 5H18V4C18 2.89543 17.1046 2 16 2H8.00003ZM16 5V4H8.00003V5H16ZM5.15979 7H18.8403L18.29 14.1534C18.2099 15.1954 17.341 16 16.2959 16H7.70416C6.65908 16 5.7902 15.1954 5.71005 14.1534L5.15979 7ZM11 21C11 22.1046 10.1046 23 9 23C7.89543 23 7 22.1046 7 21C7 19.8954 7.89543 19 9 19C10.1046 19 11 19.8954 11 21ZM16 23C17.1046 23 18 22.1046 18 21C18 19.8954 17.1046 19 16 19C14.8954 19 14 19.8954 14 21C14 22.1046 14.8954 23 16 23Z"
              fill={color}
            />
            <circle
              r="4"
              cy="5"
              cx="19"
              fill="var(--cinnabar)"
              stroke="white"
              strokeWidth="2"
            />
          </Fragment>
        ) : (
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M8.00003 1C6.89546 1 6.00003 1.89543 6.00003 3V4H5.15979C3.99477 4 3.07632 4.99181 3.16568 6.1534L3.71594 13.3068C3.87625 15.3908 5.61401 17 7.70416 17H16.2959C18.3861 17 20.1238 15.3908 20.2841 13.3068L20.8344 6.15339C20.9237 4.9918 20.0053 4 18.8403 4H18V3C18 1.89543 17.1046 1 16 1H8.00003ZM16 4V3H8.00003V4H16ZM5.15979 6H18.8403L18.29 13.1534C18.2099 14.1954 17.341 15 16.2959 15H7.70416C6.65908 15 5.7902 14.1954 5.71005 13.1534L5.15979 6ZM11 20C11 21.1046 10.1046 22 9 22C7.89543 22 7 21.1046 7 20C7 18.8954 7.89543 18 9 18C10.1046 18 11 18.8954 11 20ZM16 22C17.1046 22 18 21.1046 18 20C18 18.8954 17.1046 18 16 18C14.8954 18 14 18.8954 14 20C14 21.1046 14.8954 22 16 22Z"
            fill={color}
          />
        )}
      </svg>
    </React.Fragment>
  );
};

Cart.propTypes = propTypes;
Cart.defaultProps = defaultProps;

export default Cart;
