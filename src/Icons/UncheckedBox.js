import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  variant: PropTypes.oneOf(["round", "default"]),
  size: PropTypes.number,
  color: PropTypes.string,
};

const defaultProps = {
  size: 24,
  variant: null,
  color: "var(--black-pearl)",
};

const UncheckedBox = ({ color, variant, size, ...props }) => {
  return (
    <React.Fragment>
      <svg
        fill="none"
        viewBox="0 0 24 24"
        width={size}
        height={size}
        xmlns="http://www.w3.org/2000/svg"
        {...props}
      >
        {variant === "round" ? (
          <circle r="9" cx="12" cy="12" strokeWidth="2" stroke={color} />
        ) : (
          <rect
            x="4"
            y="4"
            rx="3"
            width="16"
            height="16"
            strokeWidth="2"
            stroke={color}
          />
        )}
      </svg>
    </React.Fragment>
  );
};

UncheckedBox.propTypes = propTypes;
UncheckedBox.defaultProps = defaultProps;

export default UncheckedBox;
