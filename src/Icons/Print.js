import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Print = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9 4H15V6H9V4ZM7 6V4C7 2.89543 7.89543 2 9 2H15C16.1046 2 17 2.89543 17 4V6H20C21.1046 6 22 6.89543 22 8V16C22 17.1046 21.1046 18 20 18H17V20C17 21.1046 16.1046 22 15 22H9C7.89543 22 7 21.1046 7 20V18H4C2.89543 18 2 17.1046 2 16V8C2 6.89543 2.89543 6 4 6H7ZM7 16V13C7 11.8954 7.89543 11 9 11H15C16.1046 11 17 11.8954 17 13V16H20V8H17H15H9H7H4V16H7ZM9 13H15V20H9V13Z"
      fill={color}
    />
  </svg>
);

Print.propTypes = propTypes;
Print.defaultProps = defaultProps;

export default Print;
