import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Posting = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      stroke={color}
      strokeWidth="2"
      strokeLinejoin="round"
      d="M8 13.0769L4.21076 11.1337C3.40937 10.7228 3.51709 9.54487 4.37973 9.28608L20.2212 4.53365C20.984 4.3048 21.6952 5.01601 21.4664 5.77882L17.8883 17.7058C17.6952 18.3494 16.9349 18.6232 16.3757 18.2505L13 16M8 13.0769V17.9194C8 18.7579 8.96993 19.2241 9.62469 18.7002L13 16M8 13.0769L13 16"
    />
  </svg>
);

Posting.propTypes = propTypes;
Posting.defaultProps = defaultProps;

export default Posting;
