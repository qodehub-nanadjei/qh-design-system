import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const ArrowUp = ({ size, color, ...props }) => (
  <svg
    fill="none"
    viewBox="0 0 24 24"
    width={size}
    height={size}
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      d="M5 10L12 3M12 3L19 10M12 3L12 21"
      stroke={color}
    />
  </svg>
);

ArrowUp.propTypes = propTypes;
ArrowUp.defaultProps = defaultProps;

export default ArrowUp;
