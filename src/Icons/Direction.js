import React from "react";
import PropTypes from "prop-types";

/**
 * props definition
 */
const propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
};

const defaultProps = {
  size: 24,
  color: "var(--black-pearl)",
};

const Reshuffle = ({ size, color, ...props }) => {
  return (
    <svg
      fill="none"
      width={size}
      height={size}
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M3.20634 7.69505L19.8475 2.65228C20.6766 2.40103 21.4112 3.25119 21.0423 4.0351L13.3383 20.406C12.8958 21.3464 11.4904 21.0714 11.435 20.0336L11.0378 12.6065C11.0151 12.1831 10.728 11.82 10.3212 11.7005L3.21433 9.61149C2.26117 9.33131 2.25554 7.98317 3.20634 7.69505Z"
        fill={color}
      />
    </svg>
  );
};

Reshuffle.propTypes = propTypes;
Reshuffle.defaultProps = defaultProps;

export default Reshuffle;
