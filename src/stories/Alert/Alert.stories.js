import React from 'react';
import Alert from '../../Components/Alert';
import Info from '../../Icons/Info';
import Grid from '../../Components/Grid';

export default {
  title: 'Alert Banner',
  component: Alert
};

const Template = (args) => <Alert {...args} />;

export const AlertBanner = () => (
  <div className='nav-tabs-container hide-scroll'>
    <Grid sm='1fr'>
      <section className='primary'>
        <p className='title__primary mb-2'>Primary</p>
        <Grid gap='1rem'>
          <Alert variant='primary'>Alert variant primary</Alert>
          <Alert variant='primary'>
            <Info variant='fill' />
            Alert with Icon
          </Alert>
          <Alert variant='primary' withBorder>
            <Info variant='fill' />
            Alert with Icon and Border
          </Alert>
        </Grid>
      </section>
      <section className='primary'>
        <p className='title__primary mb-2'>Primary</p>
        <Grid gap='1rem'>
          <Alert variant='primary'>Alert variant primary</Alert>
        </Grid>
      </section>
      <section className='success'>
        <p className='title__success mb-2'>Success</p>
        <Grid gap='1rem'>
          <Alert variant='success'>Alert variant success</Alert>
        </Grid>
      </section>
      <section className='warning'>
        <p className='title__warning mb-2'>Warning</p>
        <Grid gap='1rem'>
          <Alert variant='warning'>Alert variant warning</Alert>
        </Grid>
      </section>
      <section className='danger'>
        <p className='title__danger mb-2'>Danger</p>
        <Grid gap='1rem'>
          <Alert variant='danger'>Alert variant danger</Alert>
        </Grid>
      </section>
      <section className='danger'>
        <p className='title__danger mb-2'>Danger Dismissable</p>
        <Grid gap='1rem'>
          <Alert variant='danger' dismissable>
            Dismissable Alert variant danger
          </Alert>
        </Grid>
      </section>
    </Grid>
  </div>
);

export const AlertBannerProps = Template.bind({});
AlertBannerProps.args = {
  color: '',
  opacity: 0.1,
  withBorder: 'false',
  children: '',
  variant: 'primary',
  dismissable: 'true'
};
