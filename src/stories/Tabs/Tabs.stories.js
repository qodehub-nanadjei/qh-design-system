import React from 'react';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

export default {
  title: 'Tabs',
  component: Tabs
};

export const NavTabs = () => {
  return (
    <div className='nav-tabs-container hide-scroll'>
      <Tabs defaultActiveKey='first' id='tabs'>
        <Tab eventKey='first' title='First' />
        <Tab eventKey='second' title='Second' />
        <Tab eventKey='third' title='Third' />
        <Tab eventKey='fourth' title='Fourth' />
        <Tab eventKey='five' title='Five' />
        <Tab eventKey='six' title='Six' />
      </Tabs>
    </div>
  );
};

// export const Template = () => <NavTabs />;
