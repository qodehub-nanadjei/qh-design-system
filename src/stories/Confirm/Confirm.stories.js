import React from 'react';
import { toast } from 'react-toastify';
import Notify from '../../Components/Notify';
import Button from '../../Components/Button';
import Grid from '../../Components/Grid';
import Confirm from '../../Components/Confirm';

export default {
  title: 'Confirm',
  component: Confirm
};

export const Confirms = () => {
  const handleConfirm = async (e) => {
    e === 'default' &&
      (await Confirm({
        msg: 'Are you sure you want to confirm these records?'
      }).catch((err) => {
        console.error('Cancelled');
      }));

    if (e) {
      toast(<Notify body='No records exist' type='error' />);
      return;
    }

    if (
      await Confirm({
        header: 'Confirm approval',
        msg: 'Are you sure you want to approve these records?',
        buttons: {
          proceed: {
            children: 'Approve'
          }
        }
      })
    ) {
      console.log('Approved');
      toast(<Notify body='All records have been approved' type='success' />);
    } else {
      console.log('Cancelled approval');
    }
  };

  return (
    <Grid sm='repeat(3, 1fr)'>
      <Button onClick={() => handleConfirm('default')}>Confirm</Button>
      <Button onClick={() => handleConfirm()}>Confirm (custom)</Button>
    </Grid>
  );
};

const Template = (args) => <Confirm {...args} />;

export const ConfirmsProps = Template.bind({});
ConfirmsProps.args = {
  show: '',
  header: '',
  msg: '',
  proceed: ''
};
