import React from 'react';
import JoeAvatar from '../../../example/src/img/joe.jpg';
import Elliot from '../../../example/src/img/elliot.jpg';
import Veronika from '../../../example/src/img/veronika.jpg';
import Label from '../../Components/Label';

export default {
  title: 'Label',
  component: Label
};

export const Labels = () => {
  return (
    <div>
      <Label className='pb-6' avatarUrl={JoeAvatar} textColor='var(--dark)'>
        Elvis
      </Label>

      <div className='d-flex'>
        <Label
          className='pr-6'
          bgColor='var(--success)'
          avatarUrl={Elliot}
          textColor='var(--white)'
          detail='Agent'
        >
          Elliot
        </Label>

        <Label
          className='pr-6'
          bgColor='var(--info)'
          avatarUrl={Veronika}
          textColor='var(--white)'
          detail='Friend'
          dismissable
        >
          Joe
        </Label>
        <Label
          className='pr-6'
          bgColor='var(--warning)'
          avatarUrl={JoeAvatar}
          textColor='var(--white)'
          detail='Admin'
        >
          Joe
        </Label>
      </div>
    </div>
  );
};

const Template = (args) => <Label {...args} />;

export const ConfirmsProps = Template.bind({});
ConfirmsProps.args = {
  bgColor: '',
  textColor: '',
  avatarUrl: '',
  detail: '',
  dismissable: '',
  children: ''
};
