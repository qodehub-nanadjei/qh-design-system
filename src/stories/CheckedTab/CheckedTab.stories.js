import React, { Fragment, useState } from 'react';

import { toast } from 'react-toastify';
import Form from '../../Icons/Form';
import Copy from '../../Icons/Copy';
import Date from '../../Icons/Date';
import Check from '../../Icons/Check';
import Grid from '../../Components/Grid';
import Button from '../../Components/Button';
import ArrowDown from '../../Icons/ArrowDown';
import Direction from '../../Icons/Direction';
import CheckedBox from '../../Icons/CheckedBox';
import Appointment from '../../Icons/Appointment';
import ChevronRight from '../../Icons/ChevronRight';
import CheckedTab from '../../Components/CheckedTab';

export const CheckedTabs = () => {
  const [active, setActive] = useState(0);

  const progress = (type) => {
    type
      ? type === 'prev'
        ? active !== 0
          ? setActive((key) => key - 1)
          : toast('No progress exists to go back')
        : type === 'next'
        ? active !== 6
          ? setActive((key) => key + 1)
          : setActive(0)
        : console.log('no type exist after next')
      : console.log('no type specified');
  };

  return (
    <Fragment>
      <section className='mb-12'>
        <h3>Variant sm</h3>
        <CheckedTab
          activeKey={active}
          variant='sm'
          tabs={[
            {
              title: 'First progress',
              desc: 'First progress description',
              icon: ChevronRight
            },
            {
              title: 'Progress 2',
              icon: Appointment
            },
            {
              title: 'Progress 3',
              desc: 'description 3',
              icon: Appointment
            },
            {
              title: 'Progress 4',
              desc: 'description 4',
              icon: Appointment
            },
            {
              title: 'Progress 5',
              desc: 'description 5',
              icon: Appointment
            },
            {
              title: 'Progress 6',
              desc: 'description for item 6',
              icon: Appointment
            }
          ]}
        />
        <Grid className='my-3' sm='repeat(2, 1fr)'>
          <Button onClick={() => progress('prev')}>Previous</Button>
          <Button onClick={() => progress('next')}>Next</Button>
        </Grid>
      </section>

      <section className='my-12'>
        <h3>Variant lg</h3>
        <CheckedTab
          activeKey={active}
          variant='lg'
          tabs={[
            {
              title: 'First progress',
              desc: 'First progress description',
              icon: ArrowDown
            },
            {
              title: 'Progress 2',
              icon: Direction
            },
            {
              title: 'Progress 3',
              desc: 'description 3',
              icon: Appointment
            },
            {
              title: 'Progress 4',
              desc: 'description 4',
              icon: Appointment
            },
            {
              title: 'Progress 5',
              desc: 'description 5',
              icon: Appointment
            },
            {
              title: 'Progress 6',
              desc: 'description for item 6',
              icon: Appointment
            }
          ]}
        />

        <Grid className='my-3' sm='repeat(2, 1fr)'>
          <Button onClick={() => progress('prev')}>Previous</Button>
          <Button onClick={() => progress('next')}>Next</Button>
        </Grid>
      </section>

      <section className='my-12'>
        <h3>Variant icon</h3>
        <CheckedTab
          activeKey={active}
          variant='icon'
          tabs={[
            {
              title: 'First progress',
              desc: 'First progress description',
              icon: Check
            },
            {
              title: 'Progress 2',
              icon: Direction
            },
            {
              title: 'Progress 3',
              desc: 'description 3',
              icon: CheckedBox
            },
            {
              title: 'Progress 4',
              desc: 'description 4',
              icon: Copy
            },
            {
              title: 'Progress 5',
              desc: 'description 5',
              icon: Date
            },
            {
              title: 'Progress 6',
              desc: 'description for item 6',
              icon: Form
            }
          ]}
        />

        <Grid className='my-3' sm='repeat(2, 1fr)'>
          <Button onClick={() => progress('prev')}>Previous</Button>
          <Button onClick={() => progress('next')}>Next</Button>
        </Grid>
      </section>
    </Fragment>
  );
};

export const Template = () => <CheckedTabs />;
