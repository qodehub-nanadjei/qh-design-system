import React from 'react';
import ChevronDown from '../../Icons/ChevronDown';
import Dropdown from 'react-bootstrap/Dropdown';

export default {
  title: 'Dropdown',
  component: Dropdown
};

export const Dropdowns = () => {
  return (
    <Dropdown>
      <Dropdown.Toggle>
        <span className='mr-1'>Dropdown</span>
        <ChevronDown />
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Dropdown.Item>Hello world This is really good</Dropdown.Item>
        <Dropdown.Item>Hi world</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

const Template = () => <Dropdown />;
